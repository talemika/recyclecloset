from django.contrib import admin
from models import *
# Register your models here.
admin.site.register(MyUser)
admin.site.register(Seller)
admin.site.register(Buyer)
admin.site.register(Product)
admin.site.register(BuyerReview)
admin.site.register(SellerReview)
admin.site.register(Message)
admin.site.register(Category)
admin.site.register(Brand)


